from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'core.views.new_home', name='home'),
    url(r'^sobre/$', 'core.views.pinheiros', name='pinheiros'),
    url(r'^servicos/$', 'core.views.servicos', name='servicos'),
    url(r'^cliente/$', 'core.views.cliente', name='cliente'),
    url(r'^contato/$', 'core.views.contato', name='contato'),
    url(r'^admin/', include(admin.site.urls)),
)

# -*- coding:utf-8 -*-
from django.db import models


class FotosSlider(models.Model):
    titulo = models.CharField('Nome', max_length=200)
    thumb = models.FileField('Imagem', upload_to='fotos/slider', null=True, blank=True)
    sobre = models.TextField('Conteúdo')
    link = models.URLField('Link', null=True, blank=True)
    active = models.BooleanField('Ativo?', default=True)

    def __unicode__(self):
        return self.titulo

    class Meta:
        ordering = ('?',)
        verbose_name = 'Foto para o Slider'
        verbose_name_plural = 'Fotos para o Slider'


class Documento(models.Model):
    titulo = models.CharField('Nome', max_length=200)
    slug = models.SlugField('Slug', max_length=100, unique=True)
    sobre = models.TextField('Descrição do serviço', blank=True, null=True)
    active = models.BooleanField('Ativo?', default=True)

    class Meta:
        ordering = ('?',)
        verbose_name = 'Documento'
        verbose_name_plural = 'Documentos'


class Calculo(models.Model):
    titulo = models.CharField('Nome', max_length=200)
    slug = models.SlugField('Slug', max_length=100, unique=True)
    sobre = models.TextField('Descrição do serviço', blank=True, null=True)
    active = models.BooleanField('Ativo?', default=True)
    link = models.URLField('Link', null=True, blank=True)

    class Meta:
        ordering = ('?',)
        verbose_name = 'Cálculo'
        verbose_name_plural = 'Cálculos'


class Servico(models.Model):
    titulo = models.CharField('Nome', max_length=200)
    slug = models.SlugField('Slug', max_length=100, unique=True)
    sobre = models.TextField('Descrição do serviço')
    active = models.BooleanField('Ativo?', default=True)

    def __unicode__(self):
        return self.titulo

    class Meta:
        ordering = ('titulo',)
        verbose_name = 'Serviço'
        verbose_name_plural = 'Serviços'
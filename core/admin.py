__author__ = 'luissiqueira'
# -*- coding:utf-8 -*-

from django.contrib import admin
from .models import FotosSlider, Servico, Documento, Calculo


class FotoAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'link', 'active')
    list_filter = ('active',)
    search_fields = ['titulo']


class ServicoAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('titulo',)}
    list_display = ('titulo', 'slug', 'active')
    list_filter = ('active',)
    search_fields = ['titulo']

    class Media:
        js = ('/static/js/tinymce/tinymce.min.js', '/static/js/tinymce/textarea.js')


admin.site.register(FotosSlider, FotoAdmin)
admin.site.register(Servico, ServicoAdmin)
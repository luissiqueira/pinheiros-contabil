# -*- coding:utf-8 -*-
__author__ = 'luissiqueira'

from django.template import RequestContext
from django.shortcuts import render_to_response
from django.core.mail import send_mail
from django.contrib import messages
from smtplib import SMTPException
from .models import FotosSlider, Servico, Documento, Calculo


def home(request):
    if request.method == 'POST':
        nome = request.POST.get('nome', None)
        telefone = request.POST.get('telefone', None)
        email = request.POST.get('email', None)
        mensagem = request.POST.get('mensagem', None)
        if nome and telefone and email and mensagem:
            try:
                conteudo = 'Nome: %s\nTelefone: %s\nEmail: %s\nMensagem: %s' % (nome, telefone, email, mensagem.strip())
                send_mail('Contato via site', conteudo, 'Website Pinheiro\'s Contábil<website@pinheiroscontabil.com.br>',
                          ['luissiqueirajr@gmail.com'], fail_silently=False)
                messages.success(request, 'Recebemos o seu contato. Em breve retornaremos.')
            except SMTPException:
                messages.error(request, 'Ocorreu um erro ao enviar o e-mail mas já estamos solucionando,\
                tente novamente mais tarde.')
        else:
            messages.error(request, 'Preencha os campos corretamentes.')
    return render_to_response('index.html', context_instance=RequestContext(request))


def new_home(request):
    fotosSlider = FotosSlider.objects.filter(active=True).order_by('?')
    return render_to_response('n_index.html', locals(), context_instance=RequestContext(request))


def pinheiros(request):
    return render_to_response('faheina.html', context_instance=RequestContext(request))


def cliente(request):
    return render_to_response('cliente.html', locals(),  context_instance=RequestContext(request))


def servicos(request):
    servicos = Servico.objects.filter(active=True)
    return render_to_response('servicos.html', locals(), context_instance=RequestContext(request))


def contato(request):
    if request.method == 'POST':
        nome = request.POST.get('nome', None)
        telefone = request.POST.get('telefone', None)
        email = request.POST.get('email', None)
        mensagem = request.POST.get('mensagem', None)
        if nome and telefone and email and mensagem:
            try:
                conteudo = 'Nome: %s\nTelefone: %s\nEmail: %s\nMensagem: %s' % (nome, telefone, email, mensagem.strip())
                send_mail('Contato via site', conteudo, 'Website Pinheiro\'s Contábil<website@pinheiroscontabil.com.br>',
                          ['luissiqueirajr@gmail.com', 'fagner314@gmail.com'], fail_silently=False)
                messages.success(request, 'Recebemos o seu contato. Em breve retornaremos.')
            except SMTPException as e:
                print e.message
                messages.error(request, 'Ocorreu um erro ao enviar o e-mail mas já estamos solucionando,\
                tente novamente mais tarde.')
        else:
            messages.error(request, 'Preencha os campos corretamentes.')
    return render_to_response('contato.html', context_instance=RequestContext(request))
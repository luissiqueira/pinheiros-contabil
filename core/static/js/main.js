$(function() {
    $('.menu li').hover(
        function(){
            if( !$(this).hasClass('active') )
                $(this).addClass('hover');
        },
        function(){
            if( $(this).hasClass('hover') )
                $(this).removeClass('hover');
        }
    );
    
    actual_page = 0
    var slider_roove = function(){
        window.clearTimeout(sliderTimeOut)
        var total = $('.item_slider').size()
        actual_page = actual_page < total - 1 ? actual_page + 1 : 0
        $('.item_slider:first-child').stop().animate({ marginTop: ((actual_page*410)*-1)+"px"},  1000  );
        sliderTimeOut = window.setTimeout(slider_roove, 5000)
    }
    sliderTimeOut = window.setTimeout(slider_roove, 5000)
    
    $('a[href=#anterior]').click(function(){
        if(actual_page == 0 && $('.item_slider').size() > 1) actual_page = $('.item_slider').size() - 2
        else if(actual_page == 1 && $('.item_slider').size() > 2) actual_page = $('.item_slider').size() - 1
        else if(actual_page > 1) actual_page -= 2
        slider_roove()
        return false
    });
    $('a[href=#proxima]').click(function(){
        slider_roove()
        return false
    });
});

tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "700",
	height: "400",
    plugins: [
         "advlist link image lists charmap hr anchor pagebreak",
         "searchreplace code insertdatetime nonbreaking",
         "contextmenu directionality emoticons paste textcolor jbimages"
   ],
   //content_css: "/static/js/tinymce/skins/lightgray/content.min.css",
   toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons | jbimages", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
 }); 
